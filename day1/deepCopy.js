const originObject = {
    id: 1,
    data: {
        name: "nam",
        age: 18
    },
    e: NaN,
    f: [1, 2]
}

function copyArray(ori, type, copy = []) {
    for (const [index, value] of ori.entries()) {
        copy[index] = deepCopy(value);
    }
    return copy;
}


function copyObject(ori, type, copy = {}) {
    for (const [key, value] of Object.entries(ori)) {
        copy[key] = deepCopy(value);
    }
    return copy;
}


function deepCopy(ori) {
    const type = typeof(ori);
    let copy;
    switch (type) {
        case 'array':
            return copyArray(ori, type, copy);
        case 'object':
            return copyObject(ori, type, copy);

        default:
            return ori;
    }
}
const newObj = deepCopy(originObject);
newObj.data.age = 35;
console.log('origin :: ', originObject)
console.log(newObj)